mvn clean package

run project java -jar pathToBuildJarFile

server starting on port 8090


get all creating accounts 
`curl -v  http://127.0.0.1:8090/account/getAll`

deposit account 
`curl -v  -H "Content-Type: application/json" -d '{"accountNumber":"account-3","amount":"135"}' http://127.0.0.1:8090/account/deposit`

withdrawals account
`curl -v  -H "Content-Type: application/json" -d '{"accountNumber":"account-3","amount":"35"}' http://127.0.0.1:8090/account/withdrawals`

transfer 

`curl -v -H "Content-Type: application/json" -d '{"creditAccount":"account-3", "debitAccount":"account-1", "amount":"50"}' http://127.0.0.1:8090/account/transfer`