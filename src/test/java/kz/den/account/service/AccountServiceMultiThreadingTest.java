package kz.den.account.service;

import kz.den.account.TestApplication;
import kz.den.account.entity.Account;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static kz.den.account.TestUtils.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
@Slf4j
public class AccountServiceMultiThreadingTest {


    @Autowired
    private AccountService accountService;
    private ExecutorService executorService = Executors.newFixedThreadPool(10);

    @Before
    public void init() {
        accountService.create(createAccount(ACCOUNT_NUMBER_3, HUNDRED));
        accountService.create(createAccount(ACCOUNT_NUMBER_4, HUNDRED));
        accountService.create(createAccount(ACCOUNT_NUMBER_5, HUNDRED_FIFTY));
    }

    @Test
    public void transfer() throws Exception {
        CountDownLatch countDown = new CountDownLatch(40);
        runAccountTransferInMultipleThreads(countDown);
        countDown.await();

        Account account = accountService.getByNumber(ACCOUNT_NUMBER_3);
        Account account2 = accountService.getByNumber(ACCOUNT_NUMBER_4);
        Account account3 = accountService.getByNumber(ACCOUNT_NUMBER_5);

        assertEquals(HUNDRED, account.getBalance());
        assertEquals(HUNDRED, account2.getBalance());
        assertEquals(HUNDRED_FIFTY, account3.getBalance());

    }

    private void runAccountTransferInMultipleThreads(CountDownLatch countDown) {
        runThreads(countDown, ACCOUNT_NUMBER_3, ACCOUNT_NUMBER_4, ONE, 5);
        runThreads(countDown, ACCOUNT_NUMBER_4, ACCOUNT_NUMBER_3, ONE, 5);
        runThreads(countDown, ACCOUNT_NUMBER_3, ACCOUNT_NUMBER_4, ONE, 5);
        runThreads(countDown, ACCOUNT_NUMBER_4, ACCOUNT_NUMBER_3, ONE, 5);
        runThreads(countDown, ACCOUNT_NUMBER_5, ACCOUNT_NUMBER_4, ONE, 5);
        runThreads(countDown, ACCOUNT_NUMBER_4, ACCOUNT_NUMBER_5, ONE, 5);
        runThreads(countDown, ACCOUNT_NUMBER_3, ACCOUNT_NUMBER_5, ONE, 5);
        runThreads(countDown, ACCOUNT_NUMBER_5, ACCOUNT_NUMBER_3, ONE, 5);
    }

    public void runThreads(CountDownLatch countDownLatch,
                           String account,
                           String account2,
                           BigDecimal amount,
                           int countThread) {
        for (int i = 0; i < countThread; i++) {
            Integer integer = i;
            executorService.execute(() -> {
                try {
                    log.info("thread is staring {} credit account {} debit account {}", integer, account, account2);
                    accountService.transfer(account, account2, amount);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                } finally {
                    log.info("thread is finished {} credit account {} debit account {}", integer, account, account2);
                    countDownLatch.countDown();
                }
            });
        }

    }

    @After
    public void stop() {
        executorService.shutdown();
    }


}