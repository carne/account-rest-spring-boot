package kz.den.account.service;

import kz.den.account.TestApplication;
import kz.den.account.entity.Account;
import kz.den.account.exeption.InsufficientFundsException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static kz.den.account.TestUtils.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
public class AccountServiceTest {


    @Autowired
    private AccountService accountService;

    @Test
    public void createAndGet() {
        Account account = createAccount(ACCOUNT_NUMBER, HUNDRED);
        accountService.create(account);
        Account foundAccount = accountService.getByNumber(ACCOUNT_NUMBER);
        assertEquals(account.getId(), foundAccount.getId());
        assertEquals(account.getNumber(), foundAccount.getNumber());
    }

    @Test
    public void deposit() {
        Account account = createAccount("deposit-01", BigDecimal.ZERO);
        accountService.create(account);
        accountService.deposit(account.getNumber(), HUNDRED);
        Account foundAccount = accountService.getByNumber(account.getNumber());
        assertEquals(HUNDRED, foundAccount.getBalance());
    }

    @Test
    public void whenWithdrawalsSuccess() {
        Account account = createAccount("credit-01", HUNDRED);
        accountService.create(account);
        accountService.withdrawals(account.getNumber(), FIFTY);
        Account foundAccount = accountService.getByNumber(account.getNumber());
        assertEquals(FIFTY, foundAccount.getBalance());
    }

    @Test(expected = InsufficientFundsException.class)
    public void whenWithdrawalsThrowException() {
        Account account = createAccount("insufficient-01", FIFTY);
        accountService.create(account);
        accountService.withdrawals(account.getNumber(), HUNDRED);
    }

    @Test
    public void transfer() {
        Account debit = createAccount("debit-02", HUNDRED);
        Account credit = createAccount("credit-02", HUNDRED);
        accountService.create(debit);
        accountService.create(credit);
        accountService.transfer(credit.getNumber(), debit.getNumber(), FIFTY);
        Account foundDebit = accountService.getByNumber(debit.getNumber());
        Account foundCredit = accountService.getByNumber(credit.getNumber());
        assertEquals(FIFTY, foundCredit.getBalance());
        assertEquals(HUNDRED_FIFTY, foundDebit.getBalance());
    }


}