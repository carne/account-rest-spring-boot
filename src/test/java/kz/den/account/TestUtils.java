package kz.den.account;

import kz.den.account.entity.Account;

import java.math.BigDecimal;

public final class TestUtils {
    private TestUtils() {
    }

    public static final String ACCOUNT_NUMBER = "test-1";
    public static final String ACCOUNT_NUMBER_2 = "test-2";
    public static final String ACCOUNT_NUMBER_3 = "test-3";
    public static final String ACCOUNT_NUMBER_4 = "test-4";
    public static final String ACCOUNT_NUMBER_5 = "test-5";
    public static final BigDecimal ONE = getBigDecimalWithScale(1);
    public static final BigDecimal HUNDRED = getBigDecimalWithScale(100);
    public static final BigDecimal HUNDRED_FIFTY = getBigDecimalWithScale(150);
    public static final BigDecimal FIFTY = getBigDecimalWithScale(50);

    public static BigDecimal getBigDecimalWithScale(long amount) {
        return BigDecimal.valueOf(amount).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public static Account createAccount(String number, BigDecimal balance) {
        Account account = new Account();
        account.setNumber(number);
        account.setBalance(balance);
        return account;
    }
}
