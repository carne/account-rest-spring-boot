package kz.den.account.exeption;

import lombok.Getter;

@Getter
public class NotFoundAccountException extends RuntimeException {
    private static final String ERROR_MESSAGE = "account %s not found";
    private final String accountNumber;

    public NotFoundAccountException(String accountNumber) {
        super(String.format(ERROR_MESSAGE, accountNumber));
        this.accountNumber = accountNumber;
    }
}
