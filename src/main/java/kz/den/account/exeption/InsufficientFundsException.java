package kz.den.account.exeption;

import lombok.Getter;

@Getter
public class InsufficientFundsException extends RuntimeException {
    private static final String ERROR_MESSAGE = "account %s has insufficient funds";
    private final String accountNumber;

    public InsufficientFundsException(String accountNumber) {
        super(String.format(ERROR_MESSAGE, accountNumber));
        this.accountNumber = accountNumber;
    }
}
