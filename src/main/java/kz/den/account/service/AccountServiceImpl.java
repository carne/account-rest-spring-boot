package kz.den.account.service;

import kz.den.account.entity.Account;
import kz.den.account.exeption.InsufficientFundsException;
import kz.den.account.exeption.NotFoundAccountException;
import kz.den.account.repository.AccountRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    @Transactional
    public Account getByNumber(String number) {
        return findAccount(number);
    }

    @Override
    @Transactional
    public void create(Account account) {
        accountRepository.saveAndFlush(account);
    }

    @Override
    @Transactional
    public void update(Account account) {
        accountRepository.saveAndFlush(account);
    }

    @Override
    public List<Account> getAll() {
        return accountRepository.findAll();
    }

    @Override
    @Transactional
    public void deposit(String number, BigDecimal amount) {
        Account account = findAccount(number);
        account.setBalance(account.getBalance().add(amount));
        accountRepository.saveAndFlush(account);
    }

    @Override
    @Transactional
    public void withdrawals(String number, BigDecimal amount) {

        Account account = findAccount(number);
        checkSufficientFunds(amount, account.getBalance(), number);
        account.setBalance(account.getBalance().subtract(amount));
        accountRepository.saveAndFlush(account);

    }

    @Override
    @Transactional
    public void transfer(String numberFrom, String numberTo, BigDecimal amount) {
        Account debit;
        Account credit;

        // try avoiding deadlock by compare two accounts. The greater account always init first
        if (isDebitComparingGreaterCredit(numberFrom, numberTo)) {
            debit = findAccount(numberTo);
            credit = findAccount(numberFrom);
        } else {
            credit = findAccount(numberFrom);
            debit = findAccount(numberTo);
        }

        checkSufficientFunds(amount, credit.getBalance(), numberFrom);
        credit.setBalance(credit.getBalance().subtract(amount));
        accountRepository.saveAndFlush(credit);
        debit.setBalance(debit.getBalance().add(amount));
        accountRepository.saveAndFlush(debit);

    }

    private boolean isDebitComparingGreaterCredit(String numberFrom, String numberTo) {
        return numberTo.compareTo(numberFrom) > 0;
    }

    private void checkSufficientFunds(BigDecimal amount, BigDecimal balance, String number) {
        if (balance.subtract(amount).signum() == -1) {
            throw new InsufficientFundsException(number);
        }
    }

    private Account findAccount(String number) {
        return accountRepository.findByNumber(number).orElseThrow(() -> new NotFoundAccountException(number));
    }
}
