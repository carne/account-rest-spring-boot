package kz.den.account.service;

import kz.den.account.entity.Account;

import java.math.BigDecimal;
import java.util.List;

public interface AccountService {

    Account getByNumber(String number);
    void create(Account account);

    void update(Account account);

    List<Account> getAll();

    void deposit(String number, BigDecimal amount);

    void withdrawals(String number, BigDecimal amount);

    void transfer(String numberFrom, String numberTo, BigDecimal amount);

}
