package kz.den.account.controller;

import kz.den.account.dto.AccountDTO;
import kz.den.account.dto.TransferDTO;
import kz.den.account.entity.Account;
import kz.den.account.service.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/account")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }


    @GetMapping(value = "/getAll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Account>> getAll() {
        return ResponseEntity.ok(accountService.getAll());
    }

    @PostMapping(value = "/deposit")
    @ResponseStatus(value = HttpStatus.OK)
    public void deposit(@RequestBody AccountDTO account) {
        accountService.deposit(account.getAccountNumber(), account.getAmount());
    }

    @PostMapping(value = "/withdrawals", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public void withdrawals(@RequestBody AccountDTO account) {
        accountService.withdrawals(account.getAccountNumber(), account.getAmount());
    }

    @PostMapping(value = "/transfer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public void transfer(@RequestBody TransferDTO transfer) {
        accountService.transfer(transfer.getCreditAccount(), transfer.getDebitAccount(), transfer.getAmount());
    }

}
