package kz.den.account.controller;

import kz.den.account.dto.ErrorDTO;
import kz.den.account.exeption.InsufficientFundsException;
import kz.den.account.exeption.NotFoundAccountException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerExceptionHandler {
    @ExceptionHandler(value = {InsufficientFundsException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorDTO insufficientFundsException(InsufficientFundsException e) {
        return createError(e.getAccountNumber(),e);
    }
    @ExceptionHandler(value = {NotFoundAccountException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorDTO notFoundAccountException(NotFoundAccountException e) {
        return createError(e.getAccountNumber(),e);
    }
    private static ErrorDTO createError (String accountNumber, Exception e) {
        ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setAccountNumber(accountNumber);
        errorDTO.setErrorType(e.getClass().getSimpleName());
        errorDTO.setErrorDetails(e.getMessage());
        return errorDTO;
    }
}
