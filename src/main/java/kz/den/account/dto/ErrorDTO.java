package kz.den.account.dto;

import lombok.Data;

@Data
public class ErrorDTO {
    private String accountNumber;
    private String errorType;
    private String errorDetails;
}
