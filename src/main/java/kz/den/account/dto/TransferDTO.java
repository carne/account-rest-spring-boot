package kz.den.account.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransferDTO {
    private String debitAccount;
    private String creditAccount;
    private BigDecimal amount;
}
