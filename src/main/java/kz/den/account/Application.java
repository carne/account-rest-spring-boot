package kz.den.account;

import kz.den.account.entity.Account;
import kz.den.account.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import kz.den.account.properties.AccountProperties;

import java.math.BigDecimal;

@SpringBootApplication
public class Application {
    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountProperties properties;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


    @Bean
    CommandLineRunner runner () {
        return arg -> properties.getMap().forEach((number, amount) -> {
            Account account = new Account();
            account.setNumber(number);
            account.setBalance(new BigDecimal(amount));
            accountService.create(account);

        });
    }
}
